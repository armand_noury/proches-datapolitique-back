#!/usr/bin/env php
<?php
/**
*
* Téléchargement et extraction des data open data
*/
Namespace Datapol\Extract;
require(dirname(__FILE__)."/core/console/Console.php");
require(dirname(__FILE__)."/core/extract/JsonSplit.php");
use Datapol\Console\Console;
use Datapol\Bootstrap\Parameters;

Class ConsoleExtract extends Console {
  protected function dbimport_help() {
    $msg = "Importe en base de données. 1 argument obligatoire, ex. \n";
    $msg .= "\tdbimport gouv # Importe récursivement tous les éléments de la configuration gouv\n";
    $msg .= "\tdbimport gouv:senateurs # Importe uniquement la source de la configuration gouv:senateurs";
    return $msg;
  }
  function dbimport($args = array()) {
    if (! isset($args[0])) {
      die("Il manque un argument");
    } else {
      $collMap = $this->collectionMapFromArg($args[0]);
    }

    $dbHost = $this->params["database_server"]["env"][getenv("APP_ENV")]["host"];
    $dbUser = $this->params["database_server"]["env"][getenv("APP_ENV")]["user"];
    
    //$dbconn = pg_connect("host=localhost dbname=senateurs user=postgres password=db_on_docker")
    //or die('Connexion impossible : ' . pg_last_error());

    $dbconn = pg_connect("host=$dbHost user=$dbUser")
    or die('Connexion impossible : ' . pg_last_error());
    
    $query = "SELECT datname FROM pg_database;";
    $result = pg_query($query) or die('Échec de la requête : ' . pg_last_error());
    while ($line = pg_fetch_array($result, null, PGSQL_NUM)) {
        var_dump($line);
    }

    pg_close($dbconn);
    
  }
}

$argsNumber = count($argv);
$console = new ConsoleExtract();
if ($argsNumber === 1) {
  echo "Aucune commande indiquée. Commandes disponibles : \n";
  foreach ($console->getCommands() as $name => $help){
    echo "$name : $help\n";
  }
  die("");
}
$command = $argv[1];

$console->$command(array_slice($argv, 2));

/*
$splitter = new JsonSplit("data_sources/an/dosleg/Dossiers_Legislatifs_XIV.json", "data_sources/an/dosleg/Dossiers_Legislatifs_XIV");
$splitter->parentKey = $splitter->data->export->dossiersLegislatifs->dossier;
$splitter->callBackFileNameKey = function ($res) {
  return $res->dossierParlementaire->uid;
};
$splitter->split();

$toCall = "\$toto->one->two";
//echo $toto->one->two;
eval("echo $toCall;");
*/