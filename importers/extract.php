#!/usr/bin/env php
<?php
/**
*
* Téléchargement et extraction des data open data
*/
Namespace Datapol\Extract;
require(dirname(__FILE__)."/core/console/Console.php");
require(dirname(__FILE__)."/core/extract/JsonSplit.php");
use Datapol\Console\Console;
use Datapol\Bootstrap\Parameters;
use Datapol\Extract\JsonSplit;
use Symfony\Component\Yaml\Yaml;

Class ConsoleExtract extends Console {
  // Télécharge et extrait
  private function downloadFile($section, $sourceConf, $destinationConf) {
      $rootStorage =  dirname(__FILE__)."/../".$this->params["storage_path"];
      $storage = $rootStorage."/".$section."/".$destinationConf["filesystem"]["dir"]."/";
      mkdir($storage, 0755, true);
      $storage = $storage."/".$sourceConf["downloaded_filename"];
      echo shell_exec("wget -O $storage ".$sourceConf['url']);
      if ($sourceConf["compressed"] == "zip") {
        $expandedDir = preg_replace("/.json.zip/", "", $storage);
        $expandedDir = preg_replace("/.zip/", "", $expandedDir);
        echo shell_exec("rm -rf $expandedDir/*");
        echo shell_exec(dirname(__FILE__)."/../importers/bintools/unzip.sh $storage");
      }
      if (isset($destinationConf["filesystem"]["json_split"])) {
        if(isset($expandedDir)) {
          $jsonName = preg_replace("/.zip$/", "", $sourceConf["downloaded_filename"]);
          $jsonSource = $expandedDir."/".$jsonName;
        } else {
            $jsonSource = $storage;
        }
        $jsonsDest = $rootStorage."/".$section."/".$destinationConf["filesystem"]["dir"]."/".$destinationConf["filesystem"]["json_split"]["dest_path"];
        $splitter = new JsonSplit("$jsonSource", $jsonsDest);
        $parentKey = $destinationConf["filesystem"]["json_split"]["parent_key"];
        global $fileNameKey;
        $fileNameKey = $destinationConf["filesystem"]["json_split"]["filename_key"];
        eval("\$splitter->parentKey = \$".$parentKey.";");
        $splitter->callBackFileNameKey = function ($res) {
          global $fileNameKey;
          eval("\$fileName = \$".$fileNameKey.";");
          return $fileName;
        };
        $splitter->split();
        // python format $jsonsDest
        unlink($jsonSource);
      } else {
        // python format  $expandedDir
      }
  }
  // Parse l'argument de configuration (truc:bidule)

  protected function download_help() {
    $msg = "Télécharge les fichiers sources. 1 argument obligatoire, ex. \n";
    $msg .= "\tdownload gouv # Télécharge récursivement tous les éléments de la configuration gouv\n";
    $msg .= "\tdownload gouv:senateurs # Télécharge uniquement la source de la configuration gouv:senateurs";
    return $msg;
  }
  function download($args = array()) {
    if (! isset($args[0])) {
      die("Il manque un argument");
    } else {
      $collMap = $this->collectionMapFromArg($args[0]);
    }

    eval("\$conf = \$this->configs".$collMap->arrayKey.";");
    if ($collMap->level == 2) {
      if (isset($conf["source"])) {
          echo "Source item $collMap->keys : ".$conf["source"]["url"]."\n";
          $this->downloadFile($collMap->keysArray[0], $conf["source"], $conf["destination"]);
      } else {
          echo "Pas d'entrée 'source' pour l'item $collMap->keys\n";
      }
    } else {
      foreach ($conf as $itemName => $itemArray) {
        if (isset($itemArray["source"])) {
            echo "Source item $itemName : ".$itemArray["source"]["url"]."\n";
            $this->downloadFile($collMap->keysArray[0], $itemArray["source"], $itemArray["destination"]);
        } else {
            echo "Pas d'entrée 'source' pour l'item $collMap->keys\n";
        }
      }
    }
    
  }

  // Formate les noms de colonne extrapolées des nodes json
  private function sanitizeColumnName($name) {
    $name = str_replace("@", "_", $name);
    $name = str_replace(":", "_", $name);
    $name = substr($name, 0, 10);

    return $name;
  }
  protected function recurse($parent, $array){
    $this->arbo = array();
    foreach ($array as $k=>$val){
      if (is_array($val)) {
        $this->recurse($parent."-".$k,$val);
      } else {
        $this->values[$parent."-".$k] = $val;
      }
    }
  }

  protected function sqlentitygen_help() {
    $msg = array();
    $msg []= "Fabrique le sql de création de table par rapport à un json.";
    $msg []= "\targument 1 :  /chemin/fichier.json";
    $msg []= "\targument 2 :  le nœud parent json à partir duquel les champs sont récupérés";
    $msg []= "\toption :  --only-jsonpath";
    $msg []= "\toption :  --only-jsonpathvalue";
    return implode("\n", $msg);
  }
  function sqlentitygen($args = array()) {
    if (! isset($args[0])) {
      die("Il manque le premier argument");
    } elseif (! isset($args[1])) {
      die("Il manque le second argument");
    } else {
      $jsonFile = $args[0];
      $jsonParentNode = $args[1];
    }
    if (isset($args[2])) {
      $option = $args[2];
    }
    $yaml = Yaml::parse(file_get_contents($jsonFile));
    $this->values = array();
    $this->recurse($jsonParentNode, $yaml[$jsonParentNode]);
    //var_dump($this->values);
    //die("\n --- Die");
    $jsonPath = array();
    $jsonPathValue = array();
    $sqlLine = array();
    /* Parcours du tableau des valeurs trouvées
    [chemin-dans-le-json][valeur]
    */
    foreach($this->values as $columnsString=>$v){
      $columns = explode("-", $columnsString);
      $column = array();
      foreach($columns as $columnOrigName){
        // on truncate le nom de la colonne
        $column []= $this->sanitizeColumnName($columnOrigName); 
      }
      // on retire la première colonne
      $column = array_slice($column, 1);
      $column = implode("_",$column);

      $jsonPath []= str_replace("-", "->", $columnsString);
      $valueTruncated = substr($v, 0, 50);
      if(strlen($v) > 50){
        $valueTruncated = $valueTruncated."...";
      }
      $jsonPathValue []= str_replace("-", "->", $columnsString)." : ".$valueTruncated;
      $sqlLine []= $column." character varying(30) # $columnsString";
      
    }

    if($option == "--only-jsonpath") {
      echo implode("\n",$jsonPath)."\n";
    } elseif($option == "--only-jsonpathvalue") {
      echo implode("\n",$jsonPathValue)."\n";
    } else {
      echo "CREATE TABLE IF NOT EXISTS $jsonParentNode (\n\t";
      echo implode(",\n\t",$sqlLine);
      echo "\n)\n";
    }
  }
}

$argsNumber = count($argv);
$console = new ConsoleExtract();
if ($argsNumber === 1) {
  echo "Aucune commande indiquée. Commandes disponibles : \n";
  foreach ($console->getCommands() as $name => $help){
    echo "$name : $help\n";
  }
  die("");
}
$command = $argv[1];

$console->$command(array_slice($argv, 2));

/*
$splitter = new JsonSplit("data_sources/an/dosleg/Dossiers_Legislatifs_XIV.json", "data_sources/an/dosleg/Dossiers_Legislatifs_XIV");
$splitter->parentKey = $splitter->data->export->dossiersLegislatifs->dossier;
$splitter->callBackFileNameKey = function ($res) {
  return $res->dossierParlementaire->uid;
};
$splitter->split();

$toCall = "\$toto->one->two";
//echo $toto->one->two;
eval("echo $toCall;");
*/