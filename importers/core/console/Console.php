<?php
Namespace Datapol\Console;
require(dirname(__FILE__)."/../bootstrap/Bootstrap.php");
use Datapol\Bootstrap\Parameters;

Class CollectionMap {
  public $keys; // la chaîne d'argument concernant le config : truc:bidule
  public $arrayKey; // le chemin dans le tableau de config : ["truc"]["bidule"]
  public $keysArray; // tableau de chaque membre du chemin dans la config : ("truc", "bidule")
}
abstract class Console
{
  protected $params;
  protected $configs;
  protected $commands = array();

  function __construct() {
    $parameters = new Parameters();
    $this->configs = $parameters->configs;
    $this->params = $parameters->params;
    $this->setCommands();
  }

  private function setCommands() {
    foreach (get_class_methods($this) as $name) {
      $reflection = new \ReflectionMethod($this, $name);
      if ($reflection->isPublic() && $name != "__construct" && $name != "getCommands") {
          $help = $name . "_help";
          $this->commands[$name] = $this->$help();
      }
    }
    
  }
  function getCommands() {
    return $this->commands;
  }
  // Parse l'argument de configuration (truc:bidule)
  protected function collectionMapFromArg($configArg){
      $collMap = new CollectionMap();
      $collMap->keys = $configArg;
      $arrayKey = array();
      $keysArray = explode(":", $collMap->keys);
      foreach ($keysArray as $key){
        $arrayKey[] = "['".$key."']";
      }
      $arrayKey = implode("",$arrayKey);
      if (! strpos($collMap->keys, ":")) {
        $collMap->level = 1;
      } else {
        $collMap->level = 2;
      }
      $collMap->arrayKey = $arrayKey;
      $collMap->keysArray = $keysArray;
      return $collMap;
  }
}



