#!/usr/bin/env php
<?php
require_once(dirname(__FILE__)."/co.php");

// Exécution de la requête SQL
$query = 'SELECT * FROM sen LIMIT 10';
$result = pg_query($query) or die('Échec de la requête : ' . pg_last_error());

// Affichage des résultats en txt
echo "\n";
while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
    echo "\t-----\n";
    foreach ($line as $col_name => $col_value) {
        echo "\t\t$col_name : $col_value\n";
    }
    echo "\t\n";
}
echo "\n";

// Libère le résultat
pg_free_result($result);

require_once(dirname(__FILE__)."/deco.php");
?>