<?php
abstract class ImportCsv
{
  public $excludedFields;
  public $fieldsNames;

  public function setExcludedFields($excludedFields) {
    $this->excludedFields = $excludedFields;
  }
  public function getExcludedFields() {
    return $this->excludedFields;
  }
  public function getFieldsNames() {
    return $this->fieldsNames;
  }
  public function getFieldsNamesForSum() {
    return $this->fieldsNamesForSum;
  }
  public function setFieldsNames() {
    // Calcul de la somme de la ligne en excluant les champs à exclure
    $query = "SELECT column_name FROM information_schema.columns WHERE table_name = 'sen'";
    $result = pg_query($query) or die('Échec de la requête : ' . pg_last_error());
    while ($line = pg_fetch_array($result, null, PGSQL_NUM)) {
        $fieldsNames[] = $line[0];
        if( ! in_array($line[0], $this->excludedFields)) {
          $fieldsNamesForSum[] = $line[0];
        }
    }
    $this->fieldsNames = $fieldsNames;
    $this->fieldsNamesForSum = $fieldsNamesForSum;
  }
  
}



