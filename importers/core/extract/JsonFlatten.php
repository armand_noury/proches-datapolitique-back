<?php

namespace DataPol\Extract;
Class JsonFlatten {

  protected function recurse($parent, $array){
    foreach ($array as $k=>$val){
      if (is_array($val)) {
        $this->recurse($parent."-".$k,$val);
      } else {
        $this->values[$parent."-".$k] = $val;
      }
    }
  }
}

