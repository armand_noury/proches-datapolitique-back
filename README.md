## Rappel droit constit

Initiateur versus rapporteur
Pour chaque texte tu as le nom de la personne qui en est à l'initiative : soit c'est un parlementaire (un député ou un sénateur) et dans ce cas on parle d'une proposition de loi, soit le gouvernement et dans ce cas c'est un projet de loi. 
Et ensuite, à chaque passage en chambre, le texte est représenté par un parlementaire qu'on appelle le rapporteur.

